

    /*

        Документация:
        
        https://developer.mozilla.org/ru/docs/HTML/HTML5/Constraint_validation
        
        form.checkValidity() > Проверка всех полей формы на валидость
        form.reportValidity() > Проверяет все поля на валидность и выводит возле каждого из не прошедшего валидацию
            сообщение с ошибкой

        formElement.validity > Объект с параметрами валидности поля 
        formElement.setCustomValidity(message) > Метод который задаст validity.valid = false, и при попытке отправки
            сообщения выведет message в браузерный попал

        Классы для стилизации состояния элемента
        input:valid{}
        input:invalid{}

        
        Задание:
        
        Используя браузерное API для валидации форм реализовать валидацию следующей формы:
        

        - Имя пользователя: type:text -> validation: required; minlength = 2;  
            Если пустое выввести сообщение: "Как тебя зовут дружище?!"
        - Email: type: email -> validation: required; minlength = 3; validEmail;
            Если эмейл не валидный вывести сообщение "Ну и зря, не получишь бандероль с яблоками!"
        - Пароль: type: password -> validation: required; minlength = 8; maxlength=16;
            Если пустой вывести сообщение: "Я никому не скажу наш секрет";
        - Количество сьеденых яблок: type: number -> validation: required; minlength = 1; validNumber;
            Если количество 0 вывести эррор с сообщением "Ну хоть покушай немного... Яблочки вкусные"
        - Напиши спасибо за яблоки: type: text -> validation: required; 
            Если текст !== "спасибо" вывести эррор с сообщением "Фу, неблагодарный(-ая)!" используя setCustomValidity();

        - Согласен на обучение: type: checkbox -> validation: required;
            Если не выбран вывести эррор с сообщение: "Необразованные живут дольше! Хорошо подумай!"

        Внизу две кнопки:

        1) Обычный submit который отправит форму, если она валидна.
        2) Кнопка Validate(Проверить) которая запускает методы:
            - yourForm.checkValidity: и выводит на страницу сообщение с результатом проверки
            - yourForm.reportValidity: вызывает проверку всех правил и вывод сообщения с ошибкой, если такая есть

    */
    addEventListener('DOMContentLoaded', () => {

    	var form = document.getElementById('myForm')
        var submit = document.getElementById('submit');
        var validate = document.getElementById('validate');
     
       

        
        var username = document.forms.myForm.elements.username;
        var email = document.forms.myForm.elements.email;
        var password = document.forms.myForm.elements.password;
        var numberOfApple = document.forms.myForm.elements.numberOfEatApple;
        var writeThk = document.forms.myForm.elements.writeThnk;
        var checkbox = document.forms.myForm.elements.agreement;

        validate.addEventListener('click', function () {
       		if (!form.reportValidity()) {
       			alert('Incorrect input')
       		}
    	});

    	validate.addEventListener('click', function (e) {
    		e.preventDefault();
    		result.innerHTML = '';
        	if (!form.checkValidity()) {
        	
	        	var elem = form.getElementsByTagName('input');
	        	var elemArr = Array.from(elem);
	        	
	        	elemArr.forEach( function (item) {
	   				
	   				if (item.type !== 'submit') {
	   					if (!item.validity.valid) {

	        			var span = document.createElement('span');
		        			span.innerText = 'ERROR '+ item.name;
		        			span.style.color = 'red';
		        			result.appendChild(span)


	        			} else {
	        			var span = document.createElement('span');
		        			span.innerText = 'OK '+ item.name;
		        			span.style.color = 'green';
		        			result.appendChild(span)
	        			}

	   				}
	        		
	        	  });
            }

    	});

    	
        username.setCustomValidity('Как тебя зовут дружище!?');
        username.addEventListener('input', function () {
            if (username.value === '') {
                username.setCustomValidity('Как тебя зовут дружище!?')
            } else {
                username.setCustomValidity('')
            }
        }); 

        email.setCustomValidity('Ну и зря, не получишь бандероль с яблоками!');
        email.addEventListener('input', () => {
             email.setCustomValidity('');
            if (!email.validity.valid) {   
             email.setCustomValidity('Ну и зря, не получишь бандероль с яблоками!');
            } 
        }); 
        
        password.setCustomValidity('Я никому не скажу наш секрет');
        password.addEventListener('input', () => {
            if (password.value === '') {
                password.setCustomValidity('Я никому не скажу наш секрет');
            } else {
                password.setCustomValidity('')
            }
        });

        numberOfApple.setCustomValidity('Ну хоть покушай немного... Яблочки вкусные');
        numberOfApple.addEventListener('input', () => {
            if (numberOfApple.value <= 0) {
                numberOfApple.setCustomValidity('Ну хоть покушай немного... Яблочки вкусные');
            } else {
                numberOfApple.setCustomValidity('');
            }
        });

        writeThk.setCustomValidity('Фу, неблагодарный(-ая)!');
        writeThk.addEventListener('input', () => {
            if (writeThk.value !== 'спасибо') {
                writeThk.setCustomValidity('Фу, неблагодарный(-ая)!');
            } else {
                writeThk.setCustomValidity('');
            }
        });

        checkbox.setCustomValidity('Необразованные живут дольше! Хорошо подумай!');
        checkbox.addEventListener('input', () => {
            if (checkbox.checked) {
                checkbox.setCustomValidity('');;
            } else {
            	checkbox.setCustomValidity('Необразованные живут дольше! Хорошо подумай!');
            }
        });
    });
    
 