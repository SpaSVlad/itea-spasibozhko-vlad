document.addEventListener('DOMContentLoaded', ()=> {

	const result = document.querySelector('.result_post');
	const sendPostButton = document.getElementById('send');
	const form = document.getElementById('create');
	const buttonLoadImage = document.getElementById('progres_img');
	const loadedImg = document.getElementById('loaded_img');
	const uploadFile = document.querySelector('.file_load');
	const image_url = document.getElementById('image_url');
	
	
	let dataPostArr = [];

	class Post {
		constructor (name, text, image) {
			this.id = Math.floor(Math.random() * 10000000000000001);
			this.name = name;
			this.date = new Date().toDateString();
			this.text = text;
			this.image = image;
			this.likes = 0;
			this.comments = [];


			this._addlike = this.addLike.bind(this);
			this._showComments = this.showComments.bind(this);
			this._addComments = this.addComments.bind(this);
			this._sendComments = this.sendComments.bind(this);
		}

	sendComments (e) {
		let currentNode = document.getElementById(this.id);
		let currentCommentsNode = currentNode.querySelector('#create_comments');	
		let commentsAuthorName = currentCommentsNode.querySelector('input');
		let commentsText = currentCommentsNode.querySelector('textarea');

		if (commentsAuthorName.value !== '' && commentsText.value !== '') {
			let commentsObj = new Comment(commentsAuthorName.value, commentsText.value);
			this.comments.push(commentsObj);

			let localStorageData = JSON.stringify(dataPostArr);
			localStorage.setItem('post', localStorageData);

			commentsAuthorName.style.border = 'none';
			commentsText.style.border = 'none';
			currentCommentsNode.classList.toggle('form_display_none');

		} else {
			e.preventDefault()
		}	

		let commentsCounter = currentNode.querySelector('.show_comments');
			commentsCounter.innerText = `Comments (${this.comments.length})`

		let commentsNode = currentNode.querySelector('#all_comments');
		commentsNode.innerHTML = '';
		dataPostArr.map(item => {
			if (item.id === this.id) {
				item.comments.map(elem => {
					elem.render(commentsNode)
				});
			}
		});
	}

	showComments (e) {
		e.preventDefault();
		let currentNode = document.getElementById(this.id);
		let currentCommentsNode = currentNode.querySelector('#all_comments');
		let elementsCheck = currentCommentsNode.querySelector('div');

		if (elementsCheck === null) {
			dataPostArr.map(item => {
				if (item.id === this.id) {
					item.comments.map(elem => {
						elem.render(currentCommentsNode)
					})
				}
			})
		} else {
			currentCommentsNode.innerHTML = '';
		}	
	};

	addComments (e) {
		e.preventDefault();
		let currentForm = document.getElementById(this.id);
		let addNewComment = currentForm.querySelector('#create_comments');
			addNewComment.classList.toggle('form_display_none');
	};

	addLike (e) {
		e.preventDefault();
		this.likes += 1;
		
		let currentForm = document.getElementById(this.id);
		let counterLikes = currentForm.querySelector('._likes');
			counterLikes.innerHTML = `
				<i class="material-icons">favorite</i>
				Likes ${this.likes}`
	}

	render (node) {
		const div = document.createElement('div');
		div.id = this.id;
		div.innerHTML = 
		`<form id = ${this.id}>
				<div class="main_line">
					<span class="post_name_author"> ${this.name}</span>
					<span class="post_date_created"> ${this.date}</span>
				</div>
				<div class="image_post">
					<img src=${this.image}>
				</div>

				<div class="text_post">
					<p>${this.text}</p>
				</div>
				<div class="post_button">
					<button class="_likes">
						<i class="material-icons">favorite</i>
						Likes ${this.likes} 
					</button>
					<button class="_add_comments">Write Coments</button>
					<button class="show_comments">Comments (${this.comments.length})</button>
				</div>
				<div id="all_comments"></div>
				<div id="create_comments" class = "form_display_none">
					<input type="text" name="author_name" placeholder=" Your name">
					<textarea placeholder=" Your comments"  name="comment_text" rows="4" ></textarea>
					<button type="reset">Send Comments</button>
				</div> 	
			</form>
			`

			node.appendChild(div);

			const buttonLike = div.querySelector('._likes').addEventListener('click', this._addlike);
			const showCommentsButton = div.querySelector('.show_comments').addEventListener('click', this._showComments);
			const addCommentsButton = div.querySelector('._add_comments').addEventListener('click', this._addComments);
			const commentsForm = div.querySelector('#create_comments');
			const sendButtonComments = commentsForm.querySelector('button')

			sendButtonComments.addEventListener('click', this._sendComments);

			const createCommentsNode = div.querySelector('#create_comments');
			const commentAuthorName = createCommentsNode.querySelector('input');

			commentAuthorName.addEventListener('input', () => {
				if (commentAuthorName.value !== '' ) {
					commentAuthorName.style.border = '1px solid green';
					sendButtonComments.disabled = false;
				} else {
					commentAuthorName.style.border = '1px solid red';
				}
			});

			const commentText = createCommentsNode.querySelector('textarea')
				commentText.addEventListener('input', () => {
				if (commentText.value !== '') {
					commentText.style.border = '1px solid green';
					sendButtonComments.disabled = false;
				} else {
					commentText.style.border = '1px solid red';
				}
			})
		}
	}


	function sendPost (e) {
		
		let nameAuthor = form.elements.name.value;
		let textValue = form.elements.text.value;
		let img = loadedImg.querySelector('img');
		
		if (form.checkValidity()) {	
			loadedImg.innerHTML = '';

			let someObj = new Post(nameAuthor, textValue, img.src);
			someObj.render(result);
		
			
			dataPostArr.push(someObj);
			let localStorageData = JSON.stringify(dataPostArr);
			localStorage.setItem('post', localStorageData);
		} else {
			return
		}
	}

	form.addEventListener('submit', sendPost);
	
	function showImage () {
		loadedImg.innerHTML = '';
		image_url.style.border = 'none';
		const img = document.createElement('img');
			img.src = image_url.value;

			img.addEventListener('error', () => {
				image_url.style.border = '1px solid red';
			});		
		loadedImg.appendChild(img);
		successUpload();
	};

    image_url.addEventListener('change', showImage);

	function showFile () {
		loadedImg.innerHTML = '';
		const uploadFileInput = uploadFile.querySelector('input');
		const img = document.createElement('img');
		let some = event.target.files[0];

		const test = new FileReader();
		test.readAsDataURL(some);
		test.onload = function() {
			img.src = test.result;	
		}
		loadedImg.appendChild(img)
		successUpload();
	}
	
	uploadFile.addEventListener('change', showFile, false);


	function successUpload () {
		let currentImage = loadedImg.querySelector('img');
		currentImage.addEventListener('load', () => {
			sendPostButton.disabled = false;
		});

		currentImage.addEventListener('error', () => {
			sendPostButton.disabled = true;
		});
	}

	class Comment  {
		constructor (name, text, id) {
			
			this.name = name;
			this.text = text;
			this.date = new Date().toDateString();
			this.id = id;
		}
		render (commentsNode) {
			const div = document.createElement('div');
			div.className = 'current_comment'
			div.innerHTML = `
				<div>
					<span class="comments_author_name">${this.name}</span>
					<span class="comments_date_created">${this.date}</span>
				</div>
				<p>${this.text}</p>
			`
			commentsNode.appendChild(div)
		};
	};



	let checkLocalStorage = localStorage.getItem('post');

	if (checkLocalStorage !== null) {

		let dataArray = JSON.parse(checkLocalStorage).map (item => {
			let article =  new Post (item.name, item.text, item.image);
			item.comments.map(elem => {
				article.comments.push( new Comment (elem.name, elem.text, elem.id));
			});
			return article;
		});
		
		dataPostArr = dataArray;

		dataArray.map(item => {
			item.render(result);
			
			item.comments.map( elem => {
				let currentForm = document.getElementById(item.id);
				let commentsNode = currentForm.querySelector('#all_comments');
				elem.render(commentsNode)
			});
		});
		
	} else { 
		return
	};

});