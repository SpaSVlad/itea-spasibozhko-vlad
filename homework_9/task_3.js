/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/
let dataArr = [];

class Post  {
    constructor (active, title, about, likes) {
    this._id = new Date().getTime();
    this.isActive = active;
    this.title = title;
    this.about = about;
    this.likes = likes;
    this.created_at = new Date().toDateString()

    dataArr.push(this);
    this.saveToStorage()
    }

    addLike () {
        this.likes += 1;    
        let node = document.getElementById(this._id)
        this.render(node.parentNode);
        this.saveToStorage()
    }

    render(result) {
        result.innerHTML = '';
        dataArr.map (item => {
            let div = document.createElement('div');
            div.id = item._id;
            div.innerHTML = `
                <h3>${item.title}</h3>
                <p>${item.created_at}</p>
                <button>Likes ${item.likes}</button>
            `
            result.appendChild(div);
            div.querySelector('button').addEventListener('click', () => {
                item.addLike();
            })
    
        })
    }

    saveToStorage() {
        localStorage.setItem('post', JSON.stringify(dataArr))
    }
}

const result = document.getElementById('task_3');


let storageData = localStorage.getItem('post');

if (storageData !== null) {

    let class_obj = JSON.parse(storageData);
    dataArr =  class_obj.map(item => {
    return new Post (item.isActive, item.title, item.about, item.likes);
    })

}

/*
let post = new Post('1', 'velit ad proident officia nisi','3', 0);
post.render(result);

console.log(post)


let post2 = new Post ('2','velit ad proident officia nisi','4', 0);
post2.render(result);

console.log(post2)*/


dataArr.map(item => {
    item.render(result)
})
console.log(dataArr)