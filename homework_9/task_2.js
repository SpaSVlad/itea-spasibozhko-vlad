/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/ 

	document.addEventListener('DOMContentLoaded', () => {
		const result = document.getElementById('task_2')
		const mainForm = document.forms[0];
		const button = document.querySelector('input[type="submit"]');

		console.log(result)

		button.addEventListener('click', addToStorage);
		let dataArray = [];

		function addToStorage () {

			let login = mainForm.elements.login.value;
			let password = mainForm.elements.password.value;

			if (login === 'admin@example.com' && password == '12345678') {
				let dataObject = new Object(login, password);

				let dataArrayString = JSON.stringify(dataArray);
				localStorage.setItem('user', dataArrayString)
			} else {
				return
			}

		}

		function Object (name, password) {
			this.login = name;
			this.password = password;

			dataArray.push(this)
		}

		let dataStorage = localStorage.getItem('user');

		if (dataStorage !== null) {
			JSON.parse(dataStorage).map((item) => {
				console.log(item.login)

			result.innerHTML = `
				Hello ${item.login}
				<button id = 'exit'>Exit</button>
			`

			const exitButton = document.getElementById('exit');
			exitButton.addEventListener('click', removeData)
			});
		}

		function removeData () {
			localStorage.removeItem('user');
			window.location.reload()
		}

		
	})