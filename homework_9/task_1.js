/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
	
	document.addEventListener('DOMContentLoaded', () => {

		let storageColor = localStorage.getItem('currentColor')
		if (storageColor !== null ) {
			document.body.style.background = storageColor;
		}

		/*console.log(storageColor)*/
	
		const button = document.getElementById('getColor');
		button.addEventListener('click', randomColor );

		function randomColor () {

			let r = Math.floor(Math.random() * (255 - 1 + 1)) + 1;
			let g = Math.floor(Math.random() * (255 - 1 + 1)) + 1;
			let b = Math.floor(Math.random() * (255 - 1 + 1)) + 1;
			
			let color = `rgb(${r}, ${g}, ${b})`
			localStorage.setItem('currentColor', color)
			/*console.log(color)*/
		
		}

 });