/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/
  function Comment (name, text, href) {
    this.name = name;
    this.text = text;
    this.avatarUrl = href;
    this.likes = 0;
  };

  let newProt = {
    avatarUrl: 'https://blogbankir.ru/wp-content/uploads/2017/11/741.png',
    addLike: function(){
      this.likes ++;
    }   
  };

  let CommentsArray = [];

  let newComment1 = new Comment ('Peter', 'Hello'/*, 'https://blogbankir.ru/wp-content/uploads/2017/11/741.png'*/);
  let newComment2 = new Comment ('Nastya', 'Ola', 'https://hair-moda.com/wp-content/uploads/2018/02/191064_original.jpg');
  let newComment3 = new Comment ('Vova', 'Hi' /*'https://www.deti39.com/wp-content/uploads/2017/10/kiselev-vova-kvadrat.jpeg'*/);
  let newComment4 = new Comment ('Vasya', 'Op', 'https://rocketbooking.ru/upload/iblock/56b/kska8cxjoku.jpg');
  
  CommentsArray = [newComment1, newComment2, newComment3, newComment4];
  

 let result = document.getElementById('result');

  function commentOutput (item) {
    item.forEach( function (obj) {
      Object.setPrototypeOf(obj, newProt);
      let div = document.createElement('div');
      let spanName = document.createElement('span');
      let spanText = document.createElement('span');
      let spanLikes = document.createElement('span');
      let img = document.createElement('img');

      spanName.innerText = obj.name;
      spanText.innerText = obj.text;
      spanLikes.innerText = 'likes '; 

      let test = obj.addLike.bind(obj);
      spanLikes.addEventListener('click',  function () {
        console.log('click')
        test();  
        spanLikes.innerText = 'likes ' + obj.likes;    
      });
      
      if (obj.avatarUrl == undefined) {
          delete obj.avatarUrl;
          img.src = obj.avatarUrl;
      } else {
        img.src = obj.avatarUrl
      }
     
      div.appendChild(spanName);
      div.appendChild(spanText);
      div.appendChild(spanLikes);
      div.appendChild(img);
      result.appendChild(div);
    });
  };
  commentOutput(CommentsArray);
