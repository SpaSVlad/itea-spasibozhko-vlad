/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
	
	var train = {
		name: 'Dream',
		speed: 122,
		passangers: 100 ,
		drive,
		stand,
		getPapassangers
	};


	function drive () {
		console.log('Поезд' + this.name + 'везет' + this.passangers + 'со скоростью' + this.speed)
	};

	function stand () {
		console.log('Поезд' + this.name + 'остановился. Скорость' + this.speed)
	};
	
	function getPapassangers (a) {
		console.log(this.passangers + a);
	}
