/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.




    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/
  
  function encryptCesar (word, number) {
    var arrLetter = word.split('');
    var encryptArrNumbers = arrLetter.map(function (item) {
     return item.charCodeAt() + number;
    });
    var encryptArrLetters = encryptArrNumbers.map(function (item) {
      return String.fromCharCode(item);
    })
    /*console.log(encryptLetter);*/
    var result = encryptArrLetters.reduce(function (sum,i) {
      return sum + i;
    })
    alert(result);
  };

  
  /*encryptCesar( 'hello', 3);*/

  function decryptCesar (word, number) {
    var arrLetter = word.split('');
    var decryptArrNumbers = arrLetter.map(function (item) {
     return item.charCodeAt() - number;
    });
    var decryptArrLetters = decryptArrNumbers.map(function (item) {
      return String.fromCharCode(item);
    })
    /*console.log(decryptLetter);*/
    var result = decryptArrLetters.reduce(function (sum,i) {
      return sum + i;
    })
    alert(result);
  }
  decryptCesar ('khoor', 3);


 

