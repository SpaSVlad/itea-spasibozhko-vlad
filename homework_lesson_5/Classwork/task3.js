/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода, status)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства


    Dog {
      name: '',
      breed: '',
      status: 'idle',

      changeStatus: function(){...},
      showProps: function(){...}
    }

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }
*/
 function Dog (name, bread) {
  this.name = name;
  this.bread = bread;
  this.status = 'idle';
  this.changeStatus = function () {

	    var statusArr = ['sleep', 'eat', 'run', 'walk', 'sit'];

	     function getRandomIntInclusive(min, max) {
	     min = Math.ceil(min);
	     max = Math.floor(max);
	     return Math.floor(Math.random() * (max - min + 1)) + min;
	   };

	   this.status = statusArr[getRandomIntInclusive(0, 4)];
	 };

   this.showProps = function () {
    for (var key in this) {
       console.log(key);
   };
  };

 };

 var newDog = new Dog ('Barbos', 'Pitbul');
  newDog.changeStatus(); 
  newDog.showProps();
  console.log(newDog);

     
