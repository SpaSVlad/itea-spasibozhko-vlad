
/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/
let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
let url2 = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2'

function testFunc (elem) {
	return obj = elem.json();
}

function test2Func (item) {
	let some = item.map((obj) => {
		return obj.name
	})

	return some;
}

function friendsResponse (elem) {
	  return obj = elem.json()
	 
}

function func (elem) {
	console.log(elem);
	let div = document.createElement('div');
		div.innerHTML = `<b>Username:</b> ${elem.name} <br>`
		
		elem.friendsArr.map((item) => {
	let span = document.createElement('span');
		span.innerHTML = `
		<b>Friend:</b> <i>${item}</i> <br>`
		div.appendChild(span)
	})
	document.body.appendChild(div)
	
	
}

  fetch(url, {method:'GET'})
    .then(testFunc)
    .then(test2Func)
    .then( res => {
       console.log(res)
       return fetch (url2)
        .then(friendsResponse)

        .then( res1 => {
        	

        	let friendsArr = res1[0].friends.map((item) => {       		
        			return item.name
        	})
        	
        	let name = res[Math.floor(Math.random() * (8 - 1 + 1)) + 1];

        	return mainObj = {name, friendsArr};
        })
    })
    .then( func)

    
