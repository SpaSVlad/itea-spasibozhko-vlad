/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.
*/
	const companyName = document.getElementById('company');
	const balanceCompany = document.getElementById('balance');
	const dateCompany = document.getElementById('date');
	const adressCompany = document.getElementById('adress');

	async function companiesList () {
		const companies = await fetch ('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
		const companiesJson = await companies.json();
		console.log(companiesJson);


		    companiesJson.map((item) => {
			const liName = document.createElement('li');
			const liBalance = document.createElement('li');
			const liDate = document.createElement('li');
			const liAddress = document.createElement('li');

				liName.innerText = `${item.company}`;
				liBalance.innerText = `${item.balance}`;
				liDate.innerHTML = `<button data-id = '${item._id}'>Show Date</button>`;
				liDate.className = item._id;
				liAddress.innerHTML = `<button data-id = '${item._id}'>Show Address</button>`
				liAddress.className = item._id;

			companyName.appendChild(liName)
			balanceCompany.appendChild(liBalance)
			dateCompany.appendChild(liDate)
			adressCompany.appendChild(liAddress)
			
			const buttonDate = dateCompany.querySelectorAll('button');
				buttonDate.forEach((elem) => {
					elem.addEventListener('click', showDate)
				})
			const buttonAddress = adressCompany.querySelectorAll('button');
				buttonAddress.forEach((elem) => {
					elem.addEventListener('click', showAddress)
				})

		})


		function showDate () {
			let li = dateCompany.getElementsByClassName(this.dataset.id);
			companiesJson.map((item) => {
				if (item._id === this.dataset.id) {
					li[0].innerHTML = `
						${item.registered}
					`
				} 
			})

		}

		function showAddress () {
			let li = adressCompany.getElementsByClassName(this.dataset.id);
			li[0].innerHTML = '';
			companiesJson.map((item) => {
				if (item._id === this.dataset.id) {
					for (key in item.address) {
						let span = document.createElement('span')
						span.innerHTML = `
							<b>${key}</b> : ${item.address[key]}, 
						`
						li[0].appendChild(span)
					}
					
				} 
			})
		}

	}

	companiesList()

